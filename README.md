# 概要

reactの公式チュートリアル触ってみる。  
webエディタ？みたいなので進める想定でかかれてるけど、手元に環境作ってみる

これ:  
https://reactjs.org/tutorial/tutorial.html

## 他チュートリアルてきなもの

これもなんかよさそう  
https://www.taniarascia.com/getting-started-with-react/  
https://reactjs.org/docs/thinking-in-react.html

## 参考サイト

https://qiita.com/icoxfog417/items/5d79b3336226aa51e30d

## デバッグツール

https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi/related?hl=ja

## 手順

```
npm install -g create-react-app
create-react-app react-example
cd react-example
mv src bak_src // デフォルトのソースコードを勉強用で退避
mkdir src
```

プロジェクト作成時に表示されたなんかtips?  
```
Success! Created react-example at /Users/k.soga/work/react/react-example
Inside that directory, you can run several commands:

  npm start
    Starts the development server.

  npm run build
    Bun
    dles the app into static files for production.

  npm test
    Starts the test runner.

  npm run eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!

We suggest that you begin by typing:

  cd react-example
  npm start

```

## メモ

基本的なこと
---
* `state` : 自身の状態を表すもの  
* `props` : 外(上位コンポーネント)から渡されるもの
    + 外部から渡される

`state`が変更される or `props`を受け取ると、`render`が呼ばれるらしい

`class Game ~`の定義は、ES6記法らしい。
----
これ以外にも、コンポーネント定義方法はある。  
参考: http://www.tohoho-web.com/ex/react.html
```
class Game extends React.Component {
    render() {
        return (
            <div className="game">
                <div className="game-board">
                    <Board />
                </div>
                <div className="game-info">
                    <div>{/* status */}</div>
                    <ol>{/* TODO */}</ol>
                </div>
            </div>
        );
    }
}

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);
```

コンポーネントの中にメソッドを定義できる。
----
`renderSquare`と`render`がメソッドみたいなものらしい
```
class Board extends React.Component {
    renderSquare(i) {
        return <Square value={i} />;
    }

    render() {
        const status = 'Next player: X';

        return (
            <div>
                <div className="status">{status}</div>
                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                </div>
                <div className="board-row">
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                </div>
                <div className="board-row">
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                </div>
            </div>
        );
    }
}
```

メソッドの引数は`props.value`で受け取れる
----
`renderSquare`の返却値の`<Square value={i}>`でSquareコンポーネントの`value`に値が設定されてる。  
設定された値は`this.props.propertyName`でとれる
```
class Square extends React.Component {
    render() {
        return (
            <button className="square" >
                {this.props.value}
            </button>
        );
    }
}

class Board extends React.Component {
    renderSquare(i) {
        return <Square value={i} />;
    }
    // 省略
}
```

`onClick`は、関数として設定しないと、レンダリングごとに呼び出される  
----
以下はクリックされたらalertが実行される
```
class Square extends React.Component {
 render() {
   return (
     <button className="square" onClick={() => alert('click')}>
       {this.props.value}
     </button>
   );
 }
}
```
以下は、レンダリングごとにalertが実行される
```
class Square extends React.Component {
 render() {
   return (
     <button className="square" onClick={alert('click')}>
       {this.props.value}
     </button>
   );
 }
}
```

Reactでは、コンポーネントに`constructor`を実装することで、値を持たせることができる。
---  
`React.Component`を継承したサブクラスの`constructor`では、必ず、`super()`から始まる  
`constructor`の中で、`this.state`プロパティを持たせることで、値を保持できる

```
class Square extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: null,
    };
  }

  render() {
    return (
      <button className="square" onClick={() => alert('click')}>
        {this.props.value}
      </button>
    );
  }
}
```

functional component
---
状態を持たず、`render`メソッドだけ持つコンポーネントを定義する時に使う  
`function`をつける
```
function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  );
}
```

javascriptのnull
---
nullは、ifで判定すると`false`になる
```
const value = null;
if(value){
}else{
 // こっちになる
}
```

`render`メソッドは、ピュアである必要がある
---
`render`メソッドの中では、
* コンポ―ネントの状態を操作しない
* DOMの読み書きをしない
* ブラウザの操作をしない
* `<div>`タグは、１つしか返せない(setみたいに返したりできない。入れ子になってるのはok)
 
```
//ダメパターン

return (
  <div className="commentBox">
    Hello, world! I am a 1st CommentBox.
  </div>
  <div className="commentBox">
    Hi, world! I am a 2nd CommentBox.
  </div>
);
```

コンポーネントの使用
---
`<CommentBox />`がコンポーネント。
`document.getElementById('content)`が、挿入先
```
React.render(
  <CommentBox />,
  document.getElementById('content')
);
```

パラメータ(props)
---
`Comment`というコンポーネントに`author`というパラメタを渡している。  
コンポーネント側で、`this.props`で参照可能  
`<Comment></Comment>`の間の文字列は、`this.props.children`で参照可能  
ただし、文字列ではないので、`toString`を使う必要がある

```
var CommentList = React.createClass({
  render: function() {
    return (
      <div className="commentList">
        <Comment author="Pete Hunt">This is one comment</Comment>
        <Comment author="Jordan Walke">This is *another* comment</Comment>
      </div>
    );
  }
});
```
